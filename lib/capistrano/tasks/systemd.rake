# frozen_string_literal: true

namespace :systemd do
  desc 'Restart backend'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      within current_path do
        execute 'sudo service kure-backend restart'
        execute 'sudo service kure-sidekiq restart'
      end
    end
  end
end
