class Api::RecoveryPasswordService
  include ActiveModel::Validations

  attr_reader :request, :token, :password, :password_confirmation

  validates :password,              presence: true, length: { minimum: 8 }, confirmation: { if: :password_confirmation }

  validates :password_confirmation, presence: true

  validates :token,                 presence: true

  def initialize request, params
    @request               = request if request

    @token                 = params[:token]

    @password              = params[:password]

    @password_confirmation = params[:password_confirmation]
  end

  def save!
    raise ActiveModel::StrictValidationFailed unless valid?

    validate_recovering # write errors in recovery_password object and save it

    if recovery_password.status == 'success'
      set_password_and_archive_code
    else
      save_recovery_password_record_and_raise
    end
  end

  def recovery_password
    @recovery_password ||= ::RecoveryPassword.new recovery_password_params
  end

  def recovery_code
    @recovery_code ||= ::RecoveryCode.find_by_token token
  end

  def user
    return @user if @user

    return if recovery_code&.user_id.nil?

    @user = recovery_code.user
  end

  #
  # validate recovery_password and write result in status
  #
  def validate_recovering
    check_attempts_quantity_today
    check_token_is_valid and return unless recovery_password.status == 'failure'
    check_token_expiration_date unless recovery_password.status == 'failure'
  end

  def check_token_is_valid
    if recovery_code.nil?
      recovery_password.status = :failure
      recovery_password.reason = :invalid_token
    else
      recovery_password.status = :success
    end
  end

  def check_token_expiration_date
    if recovery_code.expire_at && Time.zone.now.utc > recovery_code.expire_at
      recovery_password.status = :failure
      recovery_password.reason = :token_has_been_expired
    end
  end

  def check_attempts_quantity_today
    if ::RecoveryPassword.today_by_remote_ip(real_ip).count >= RecoveryPassword::ALLOWED_QUANTITY_ATTEMPTS_PER_DAY
      recovery_password.status = :failure
      recovery_password.reason = :too_many_attempts
    end
  end

  def set_password_and_archive_code
    RecoveryPassword.transaction do
      recovery_password.save!

      user.recovery_codes.update(token: nil, status: :archived)

      user.update!(password: password, status: :active)
    end
  end

  def save_recovery_password_record_and_raise
    recovery_password.save!

    @errors.add(:base, recovery_password.reason.gsub('_', " "))

    raise ActiveModel::StrictValidationFailed
  end

  def recovery_password_params
    {
      remote_ip:      real_ip,
      token:          token,
      user:           user
    }
  end

  def real_ip
    ### only HTTP_X_REAL_IP return real ip with nginx server because nginx proxing all request
    @real_ip ||= (request.headers['HTTP_X_REAL_IP'] || "127.0.0.1")
  end
end
