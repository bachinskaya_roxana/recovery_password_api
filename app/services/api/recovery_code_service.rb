class Api::RecoveryCodeService
  include ::ActiveModel::Validations

  attr_reader :email, :user, :request, :phone

  validates :email, email: true, unless: :phone

  validate :attempts_quantity

  def initialize request, user, params
    @request       = request if request

    @user          = user

    @email         = params[:email].presence

    @phone         = params[:phone].presence
  end

  def save!
    raise ::ActiveModel::StrictValidationFailed unless valid?

    ActiveRecord::Base.transaction do
      RecoveryCode.where(user: user).update(token: nil, status: :archived)

      recovery_code.save!
    end
  end

  def recovery_code
    @recovery_code ||= user.recovery_codes.build recovery_code_params
  end

  def recovery_code_params
    if email
      {
        remote_ip:     real_ip,
        email:         email,
        kind:          :email
      }
    elsif phone
      {
        remote_ip:     real_ip,
        phone:         phone,
        kind:          :phone
      }
    end
  end

  def attempts_quantity
    errors.add(:base, "too many attempts today") if too_much_attempts_from_ip? || too_much_attempts_from_credentials?
  end

  def too_much_attempts_from_ip?
    ::RecoveryCode.today.by_remote_ip(real_ip).count >= RecoveryCode::ALLOWED_QUANTITY_ATTEMPTS_PER_DAY
  end

  def too_much_attempts_from_credentials?
    today_with_credentials = ::RecoveryCode.today.by_phone(phone).count if phone
    today_with_credentials = ::RecoveryCode.today.by_email(email).count if email

    today_with_credentials >= ::RecoveryCode::ALLOWED_QUANTITY_ATTEMPTS_PER_DAY
  end

  def real_ip
    ### only HTTP_X_REAL_IP return real ip with nginx server because nginx proxing all request
    @real_ip ||= (request.headers['HTTP_X_REAL_IP'] || "127.0.0.1")
  end
end
