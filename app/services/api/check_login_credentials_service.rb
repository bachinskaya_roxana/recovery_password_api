class Api::CheckLoginCredentialsService
  include ::ActiveModel::Validations

  attr_reader :request, :params, :email, :phone, :user, :recovery_code_service

  def initialize request, params
    @request       = request if request

    @params        = params

    @email         = params[:email].presence

    @phone         = params[:phone].presence
  end

  def save!
    @errors = user.errors unless user.valid?

    @errors = recovery_code_service.errors unless build_recovery_code.valid?

    ActiveRecord::Base.transaction do
      user.save! unless user.persisted?

      recovery_code_service.save!
    end
  end

  private

  def user
    @user ||= User.find_or_initialize_by(phone: phone) if phone

    @user ||= User.find_or_initialize_by(email: email) if email

    @user
  end

  def build_recovery_code
    @recovery_code_service ||= Api::RecoveryCodeService.new(request, user, params)
  end
end
