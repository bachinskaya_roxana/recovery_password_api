class Api::RecoveryCodeValidationService
  include ::ActiveModel::Validations

  attr_reader :request, :token

  validates :token, presence: true

  # validate :past_time_from_last_validation

  validate :validations_quantity

  validate :validate_recovery_code_not_expired

  attr_reader :request

  def initialize request, params
    @request       = request if request

    @token         = params[:token].presence
  end

  def save!
    unless recovery_code_validation.save
      @errors = recovery_code_validation.errors

      raise ::ActiveModel::StrictValidationFailed
    end

    raise ActiveRecord::RecordInvalid unless valid?
  end

  private

  def past_time_from_last_validation
    last_request = ::RecoveryCodeValidation.today_by_remote_ip(real_ip).last

    if last_request && (last_request.created_at > (Time.zone.now.utc - RecoveryCodeValidation::DURATION_BETWEEN_VALIDATIONS.seconds))
      errors.add(:base, "wait some time between requests")
    end
  end

  def validations_quantity
    if ::RecoveryCodeValidation.today_by_remote_ip(real_ip).count >= RecoveryCodeValidation::ALLOWED_QUANTITY_ATTEMPTS_PER_DAY
      errors.add(:base, "too many attempts today")
    end
  end

  def recovery_code_validation
    ::RecoveryCodeValidation.new(remote_ip: real_ip, token: token)
  end

  def recovery_code
    @recovery_code ||= ::RecoveryCode.find_by(token: token)
  end

  def validate_recovery_code_not_expired
    if recovery_code && (recovery_code.expire_at < Time.now)
      errors.add(:token, "has been expired")
    end
  end

  def real_ip
    ### only HTTP_X_REAL_IP return real ip with nginx server because nginx proxing all request
    @real_ip ||= (request.headers['HTTP_X_REAL_IP'] || "127.0.0.1")
  end
end
