# frozen_string_literal: true

@user.decorate(context: {session: true}).to_json
