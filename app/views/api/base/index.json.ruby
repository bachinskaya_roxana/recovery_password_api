# frozen_string_literal: true

{
  collection: collection.decorate.as_json,
  total_pages: total_pages,
  current_page: current_page
}.to_json
