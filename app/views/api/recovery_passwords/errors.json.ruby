# frozen_string_literal: true

{
  errors: resource.errors.messages
}.to_json
