# frozen_string_literal: true

{ errors: { base: [@exception.to_s] } }.to_json
