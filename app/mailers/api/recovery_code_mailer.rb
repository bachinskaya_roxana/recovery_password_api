class Api::RecoveryCodeMailer < ApplicationMailer

  def email(recovery_code)
    @recovery_code = recovery_code

    mail(to: recovery_code.user.email,
         subject: "Recovery code")
  end
end

