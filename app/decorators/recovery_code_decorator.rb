class RecoveryCodeDecorator < Draper::Decorator
  delegate_all

  def as_json *args
    {
      recovery_password_type: kind
    }
  end
end
