class UserDecorator < Draper::Decorator
  delegate_all

  def as_json *args
    {
      id: id,
      phone: phone,
      email: email
    }
  end
end
