class Api::BaseController < ApplicationController

  skip_before_action :verify_authenticity_token
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  attr_reader :current_session

  helper_method :collection, :resource, :parent, :current_user

  def create
    build_resource

    resource.save!
  end

  def update
    resource.update!(resource_params)
  end

  def destroy
    resource.destroy!

    head :no_content
  end

  # :nocov:
  rescue_from ActionController::ParameterMissing do |exception|
    @exception = exception

    render :exception, status: :unprocessable_entity
  end

  rescue_from ActiveRecord::RecordInvalid, ActiveModel::StrictValidationFailed do
    render :errors, status: :unprocessable_entity
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    @exception = "Not found"

    render :exception, status: :not_found
  end
  # :nocov:

  private

  def user_by_email
    User.find_by_email(params[:email]) if params[:email]
  end

  def user_by_phone
    User.find_by_phone(params[:phone]) if params[:phone]
  end

  def parent
    raise NotImplementedError
  end

  def resource
    raise NotImplementedError
  end

  def resource_params
    raise NotImplementedError
  end

  def build_resource
    raise NotImplementedError
  end

  def collection
    raise NotImplementedError
  end
end
