# frozen_string_literal: true

class Api::DocsController < ApplicationController
  include Swagger::Blocks

  swagger_root do
    key :swagger, '2.0'
    info do
      key :version, '1.0.0'
      key :title, 'Sign up flow with code and recovery password API'
      contact do
        key :name, 'Roxana Bachinskaya'
      end
    end
    key :host, 'localhost:3000'
    # key :schemes, 'http'
    key :basePath, '/api'
    key :consumes, %w[application/json application/x-www-form-urlencoded]
    key :produces, ['application/json']
  end

  SWAGGERED_CLASSES = [
    ::Api::Docs::CheckLoginCredentialsDocs,
    ::Api::Docs::RecoveryCodeDocs,
    ::Api::Docs::RecoveryCodeValidationDocs,
    ::Api::Docs::RecoveryPasswordDocs,
    ::Api::Docs::Outputs::RecoveryCodeValidationOutput,
    ::Api::Docs::Outputs::UserBasicOutput,
    ::Api::Docs::Outputs::ErrorsOutput,
    self,
  ].freeze

  def index
    render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
  end
end
