class Api::RecoveryPasswordsController < Api::BaseController

  def create
    super

    head :no_content
  end

  private

  def resource
    @reset_password_service
  end

  def build_resource
    @reset_password_service ||= Api::RecoveryPasswordService.new(request, recovery_password_params)
  end

  def recovery_password_params
    params.require(:recovery_password).permit(:token, :password, :password_confirmation)
  end
end
