class Api::RecoveryCodesController < Api::BaseController

  def create
    if user
      super
    end

    head :no_content
  end

  private

  def build_resource
    @recovery_code_service ||= Api::RecoveryCodeService.new(request, user, params)
  end

  def resource
    @recovery_code_service
  end

  def user
    @user ||= user_by_email || user_by_phone
  end
end
