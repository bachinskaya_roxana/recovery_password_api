class Api::RecoveryCode::ValidationsController < Api::BaseController

  attr_reader :recovery_code

  def create
    build_resource

    if resource.save!
      render status: 200
    elsif recovery_code.nil?
      raise ActiveRecord::RecordNotFound
    end
  end

  private

  def build_resource
    @recovery_code_validation_service ||= Api::RecoveryCodeValidationService.new(request, params)
  end

  def resource
    @recovery_code_validation_service
  end

  def recovery_code
    @recovery_code ||= ::RecoveryCode.find_by(token: params[:token])
  end
end
