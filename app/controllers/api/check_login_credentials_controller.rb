class Api::CheckLoginCredentialsController < Api::BaseController

  def create
    unless user && user.active?
      build_resource

      resource.save!

      raise ActiveRecord::RecordNotFound
    end
  end

  private

  def user
    @user ||= user_by_email || user_by_phone
  end

  def build_resource
    @check_login_credentials_service ||= Api::CheckLoginCredentialsService.new(request, params)
  end

  def resource
    @check_login_credentials_service
  end
end
