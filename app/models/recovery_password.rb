class RecoveryPassword < ApplicationRecord
  belongs_to :user, optional: true

  ALLOWED_QUANTITY_ATTEMPTS_PER_DAY = 5

  validates :reason, presence: true, unless: :success?

  enum status: %i[ success failure ]
  enum reason: %i[ invalid_token token_has_been_expired too_many_attempts ]

  scope :today_by_remote_ip, -> (remote_ip) do
    where('created_at > ?', Time.now.utc.beginning_of_day).
    where(remote_ip: remote_ip).
    where.not(remote_ip: nil)
  end

end
