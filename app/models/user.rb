class User < ApplicationRecord

  validates :email, email: true, presence: true, unless: :phone?
  validates :password, confirmation: true, length: { minimum: 8 }, allow_blank: true
  has_secure_password validations: false

  enum status: %i[ incomplete active ]
  enum role: %i[ client ]
  enum gender: %i[ female male ]

  has_many :recovery_codes, dependent: :destroy
  has_many :recovery_passwords, dependent: :destroy

end
