class RecoveryCode < ApplicationRecord

  TOKEN_EXPIRATION_TIME = 5

  ALLOWED_QUANTITY_ATTEMPTS_PER_DAY = 5 # from 1 IP address

  enum kind: %i[ email phone ]
  enum status: %i[ active archived ]

  belongs_to :user

  validates :email, presence: true, unless: :phone?
  validates :phone, presence: true, unless: :email?

  before_create :setup_expire_at
  before_create :generate_token
  after_commit :send_token, on: :create

  scope :today, -> { where('created_at > ?', Time.now.utc.beginning_of_day) }
  scope :by_remote_ip, -> (remote_ip) { where(remote_ip: remote_ip).where.not(remote_ip: nil) }
  scope :by_phone, -> (phone) { where(phone: phone) }
  scope :by_email, -> (email) { where(email: email) }

  private

  def setup_expire_at
    self.expire_at = TOKEN_EXPIRATION_TIME.minutes.from_now.utc
  end

  def send_token
    Api::RecoveryCodeMailer.email(self).deliver_later if email

    Api::TwilioService.new(self).send_sms             if phone
  end

  def generate_token
    self.token = rand(10000..99999)

    generate_token if RecoveryCode.exists?(token: self.token)
  end
end
