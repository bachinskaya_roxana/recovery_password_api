class RecoveryCodeValidation < ApplicationRecord
  DURATION_BETWEEN_VALIDATIONS = 1 # in seconds
  ALLOWED_QUANTITY_ATTEMPTS_PER_DAY = 5

  scope :today_by_remote_ip, -> (remote_ip) do
    where('created_at > ?', Time.now.utc.beginning_of_day).
    where(remote_ip: remote_ip).
    where.not(remote_ip: nil)
  end
end
