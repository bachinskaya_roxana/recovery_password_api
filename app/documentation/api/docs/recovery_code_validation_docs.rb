class Api::Docs::RecoveryCodeValidationDocs
  include Swagger::Blocks

  swagger_path '/recovery_code/validation' do
    operation :post do
      key :description, 'validate recovery token and return recovery_password_method'
      key :summary, 'validate recovery token and return recovery_password_type'
      key :tags, ['Reset Password', 'Sign up']
      parameter do
        key :name, 'token'
        key :in, :formData
        key :required, true
        key :type, :string
      end
      response 200 do
        key :description, 'Code is valid'
        schema do
          key :'$ref', :RecoveryCodeValidationOutput
        end
      end
      response 404 do
        key :description, 'Code is invalid'
      end
      response 422 do
        key :description, 'Code is expired'
        schema do
          key :'$ref', :ErrorsOutput
        end
      end
    end
  end
end
