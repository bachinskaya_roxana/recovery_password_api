class Api::Docs::CheckLoginCredentialsDocs
  include Swagger::Blocks

  swagger_path '/check_login_credentials' do
    operation :post do
      key :description, 'Check if user exist and confirmed or send confirmation code'
      key :summary, 'Send confirmation code on email or sms'
      key :tags, ['Sign up']
      parameter do
        key :name, 'email'
        key :in, :formData
        key :required, false
        key :type, :string
      end
      parameter do
        key :name, 'phone'
        key :in, :formData
        key :required, false
        key :type, :string
      end

      response 200 do
        key :description, 'User exist'
        schema do
          key :'$ref', :UserBasicOutput
        end
      end
      response 404 do
        key :description, 'confirmation code has been send'
      end
      response 422 do
        key :description, 'Too many attempts today/Wrong format of email'
        schema do
          key :'$ref', :ErrorsOutput
        end
      end
    end
  end
end
