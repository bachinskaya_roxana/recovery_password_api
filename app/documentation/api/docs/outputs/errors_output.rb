# frozen_string_literal: true

class Api::Docs::Outputs::ErrorsOutput
  include Swagger::Blocks

  swagger_schema :ErrorsOutput do
    property 'errors[base]' do
      key :type, :array
      items do
        key :type, :string
      end
      key :description, 'Array of errors'
    end
    property 'errors[fieldname]' do
      key :type, :array
      items do
        key :type, :string
      end
      key :description, 'Array of errors'
    end
    key :example, errors: {
                    base: ["Email and/or password is invalid",
                            "Too many attempts"],
                    email: ["Invalid email"]
                  }
  end
end

