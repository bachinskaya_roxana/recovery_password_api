# frozen_string_literal: true

class Api::Docs::Outputs::AddressOutput
  include Swagger::Blocks

  swagger_schema :AddressOutput do
    property :title do
      key :type, :string
      key :description, 'title'
    end
    property :latitude do
      key :type, :float
      key :description, 'Latitude'
    end
    property :longitude do
      key :type, :float
      key :description, 'Latitude'
    end
  end
end
