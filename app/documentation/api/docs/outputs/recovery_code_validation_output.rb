# frozen_string_literal: true

class Api::Docs::Outputs::RecoveryCodeValidationOutput
  include Swagger::Blocks

  swagger_schema :RecoveryCodeValidationOutput do
    property :recovery_password_method do
      key :type, :string
      key :description, 'Phone or email'
    end
    key :example, recovery_password_method: 'phone/email'
  end
end
