# frozen_string_literal: true

class Api::Docs::Outputs::UserBasicOutput
  include Swagger::Blocks

  swagger_schema :UserBasicOutput do
    property :id do
      key :type, :integer
      key :format, :int64
      key :description, 'User ID'
    end
    property :email do
      key :type, :string
      key :description, 'Email'
    end
    property :phone do
      key :type, :string
      key :description, 'Phone'
    end
    key :example, id: 1,
                  email: 'tom@hotmail.com',
                  phone: '+390987865432'
  end
end
