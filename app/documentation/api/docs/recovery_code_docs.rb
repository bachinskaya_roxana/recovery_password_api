class Api::Docs::RecoveryCodeDocs
  include Swagger::Blocks

  swagger_path '/recovery_code' do
    operation :post do
      key :description, 'send/resend confirmation code or code for recovery password'
      key :summary, 'Send confirmation code on email or sms'
      key :tags, ['Reset Password']
      parameter do
        key :name, 'email'
        key :in, :formData
        key :required, false
        key :type, :string
      end
      parameter do
        key :name, 'phone'
        key :in, :formData
        key :required, false
        key :type, :string
      end

      response 204 do
        key :description, 'Send recovery code'
      end
    end
  end
end
