class Api::Docs::RecoveryPasswordDocs
  include Swagger::Blocks

  swagger_path '/recovery_password' do
    operation :post do
      key :description, 'set password for account if token is valid'
      key :summary, 'set password for account if token is valid'
      key :tags, ['Reset Password']
      parameter do
        key :name, 'recovery_password[token]'
        key :in, :formData
        key :required, true
        key :type, :string
      end
      parameter do
        key :name, 'recovery_password[password]'
        key :in, :formData
        key :required, true
        key :type, :string
      end
      parameter do
        key :name, 'recovery_password[password_confirmation]'
        key :in, :formData
        key :required, true
        key :type, :string
      end
      response 204 do
        key :description, 'Password has been set successfully'
      end
      response 422 do
        key :description, "
          base: ['token is expired']
          base: ['invalid_token']
          base: ['too many attepts']
          password: ['is too short (minimum is 8 characters)']
          password_confirmation: ['doesn\'t match password']"
        schema do
          key :'$ref', :ErrorsOutput
        end
      end
    end
  end
end
