FactoryBot.define do
  factory :recovery_code_validation do
    remote_ip { "127.0.0.1" }
    token { "MyString" }
  end
end
