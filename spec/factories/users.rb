FactoryBot.define do
  factory :user do
    phone { "+190992212221" }
    email { FFaker::Internet.email }
    status { 0 }
  end
end
