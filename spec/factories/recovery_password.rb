FactoryBot.define do
  factory :recovery_password do
    remote_ip { '127.0.0.1' }
    token { rand(10000..99999) }
    status { 'success' }
  end
end
