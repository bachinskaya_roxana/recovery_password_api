FactoryBot.define do
  factory :recovery_code do
    remote_ip { '127.0.0.1' }
    token { rand(10000..99999) }
    kind { rand(0..1) }
    expire_at { Time.now }
  end
end
