require 'rails_helper'

RSpec.describe User, type: :model do
  it { should be_a ApplicationRecord }

  it { should have_secure_password }

  it { should define_enum_for(:status).with_values(incomplete: 0, active: 1) }

  it { should have_many(:recovery_codes).dependent(:destroy) }

  it { should have_many(:recovery_passwords).dependent(:destroy) }
end
