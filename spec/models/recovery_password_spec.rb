require 'rails_helper'

RSpec.describe RecoveryPassword, type: :model do
  it { should be_a ApplicationRecord }

  it { should belong_to(:user).optional }

  it { should define_enum_for(:status).with_values([:success, :failure]) }

  describe 'recovery_password is failure' do
    subject { RecoveryPassword.new(token: '1111', status: :failure ) }
    it { should validate_presence_of :reason }
  end
end
