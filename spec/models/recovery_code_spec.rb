require 'rails_helper'

RSpec.describe RecoveryCode, type: :model do
  it { should be_a ApplicationRecord }

  it { should define_enum_for(:kind).with_values([:email, :phone]) }

  it { should define_enum_for(:status).with_values([:active, :archived]) }

  it { should belong_to(:user) }

  define 'validate presense of email if kind email' do
    let!(:recovery_code){ create(:recovery_code, kind: :email) }

    it { should validate_presence_of :email }
  end

  define 'validate presense of phone if kind phone' do
    let!(:recovery_code){ create(kind: :phone) }

    it { should validate_presence_of :phone }
  end

  it { should callback(:setup_expire_at).before :create }

  it { should callback(:generate_token).before :create }

  it { should callback(:send_token).after(:commit) }

  # private methods

  describe 'send_token' do
    it { expect { subject.send(:send_token) }.to_not raise_error }
  end

  describe 'setup_expire_at' do
    it { expect { subject.send(:setup_expire_at) }.to_not raise_error }
  end

  describe 'generate_token' do
    before { subject.send(:generate_token) }

    it { expect(subject.token.to_i).to be_between(10000, 99999).inclusive }
  end
end
