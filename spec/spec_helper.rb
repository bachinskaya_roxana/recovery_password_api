# frozen_string_literal: true

require 'simplecov'

SimpleCov.start 'rails' do
  Dir['app/*'].each do |dir|
    add_filter 'app/documentation'
    add_filter 'app/controllers/api/client/docs_controller.rb'
    add_group File.basename(dir), dir
  end
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.order = :random
end
