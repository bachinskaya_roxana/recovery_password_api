# frozen_string_literal: true
require 'rails_helper'

describe Api::RecoveryCodeMailer, type: :mailer do
  it { should be_a ApplicationMailer }

  describe '#email' do
    let(:user) { build_stubbed(:user) }

    let(:recovery_code) { build_stubbed(:recovery_code, user: user) }

    let(:mail) { described_class.email(recovery_code) }

    it { expect(mail.subject).to eq('Recovery code') }

    it { expect(mail.to).to eq([recovery_code.user.email]) }

    it { expect(mail.body.encoded).to include('verification code') }
  end
end
