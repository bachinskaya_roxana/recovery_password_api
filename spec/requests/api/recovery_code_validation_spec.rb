# frozen_string_literal: true
require 'rails_helper'

describe 'Recovery Code Validation', type: :request do
  let!(:user) { create(:user, email: "dev@mlsdev.com") }
  let(:params) {{ token: recovery_code.token }}

  let(:headers) do
    {
      'ACCEPT'            => 'application/json',
      'HTTP_CONTENT_TYPE' => 'application/json'
    }
  end

  let(:invalid_token_schema) { {errors: {base: ["Not found"]}} }

  let(:success_schema_phone) { {recovery_password_type: "phone"} }

  let(:success_schema_email) { {recovery_password_type: "email"} }

  let(:too_many_attemps_schema) { {errors: {base: ["too many attempts today"]}} }

  describe 'validation token with correct token from phone' do
    before { VCR.insert_cassette 'twilio_request' }

    let!(:recovery_code) { create(:recovery_code, phone: "+190992212221", kind: :phone, user: user) }

    it 'should return 200' do
      post '/api/recovery_code/validation', headers: headers, params: params
      expect(json_response).to match(success_schema_phone)
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'validation token with correct token' do
    let!(:recovery_code) { create(:recovery_code, email: "dev@mlsdev.com", kind: :email, user: user) }

    let(:params) {{ token: recovery_code.token }}

    it 'should return 200' do
      post '/api/recovery_code/validation', headers: headers, params: params
      expect(json_response).to match(success_schema_email)
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'recovery code validation with incorrect token' do
    let(:params) {{ token: "123" }}

    it 'should return status 404' do
      post '/api/recovery_code/validation', headers: headers, params: params
      expect(json_response).to match(invalid_token_schema)
      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'recovery code validation - too much attempts' do
    before { VCR.insert_cassette 'twilio_request' }

    let!(:recovery_code) { create(:recovery_code, email: "dev@mlsdev.com", kind: :email, user: user) }

    let(:params) {{ token: recovery_code.token }}

    5.times{|i| let!("recovery_code_validation#{i}") { create(:recovery_code_validation, token: recovery_code.token) }}

    it 'should return status 404' do
      post '/api/recovery_code/validation', headers: headers, params: params
      expect(json_response).to match(too_many_attemps_schema)
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  after { VCR.eject_cassette }
end
