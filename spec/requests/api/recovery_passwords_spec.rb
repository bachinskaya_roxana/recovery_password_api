# frozen_string_literal: true
require 'rails_helper'

describe 'Recovery Password Request', type: :request do
  let!(:user) { create(:user, email: "dev@mlsdev.com") }
  let(:recovery_code) { create(:recovery_code, phone: "+190992212221", kind: :phone, user: user) }
  let(:params) {{ recovery_password: { token: recovery_code.token, password: "q1234567", password_confirmation: "q1234567" }}}

  let(:headers) do
    {
      'ACCEPT'            => 'application/json',
      'HTTP_CONTENT_TYPE' => 'application/json'
    }
  end

  let(:invalid_token_schema) { {errors: {base: ["invalid token"]}} }

  let(:invalid_password_confirmation_schema) { {errors: {password_confirmation: ["doesn't match Password"]}} }

  let(:too_many_attemps_schema) {{ "errors" => {"base"=>["too many attempts"]}}}

  let(:short_password_schema) {{"errors" => {"password"=>["is too short (minimum is 8 characters)"]}}}

  describe 'recovery password with correct token' do
    before { VCR.insert_cassette 'twilio_request' }

    it 'should return 204' do
      post '/api/recovery_password', headers: headers, params: params
      expect(response).to have_http_status(:no_content)
    end
  end

  describe 'recovery code with incorrect token' do
    let(:params) {{ recovery_password: { token: "123", password: "q1234567", password_confirmation: "q1234567" }}}

    before { VCR.insert_cassette 'twilio_request' }

    it 'should return status 422' do
      post '/api/recovery_password', headers: headers, params: params
      expect(json_response).to match(invalid_token_schema)
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe 'recovery code with short password' do
    let(:params) {{ recovery_password: { token: "123", password: "q123", password_confirmation: "q123" }}}

    before { VCR.insert_cassette 'twilio_request' }

    it 'should return status 422' do
      post '/api/recovery_password', headers: headers, params: params
      expect(json_response).to match(short_password_schema)
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe 'recovery code with incorrect password_confirmation' do
    let(:params) {{ recovery_password: { token: "123", password: "q12345678", password_confirmation: "q1234567" }}}

    before { VCR.insert_cassette 'twilio_request' }

    it 'should return status 422' do
      post '/api/recovery_password', headers: headers, params: params
      expect(json_response).to match(invalid_password_confirmation_schema)
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe 'recovery password - too many attempts' do
    before { VCR.insert_cassette 'twilio_request' }

    let!(:recovery_code) { create(:recovery_code, email: "dev@mlsdev.com", kind: :email, user: user) }

    let(:params) {{ recovery_password: { token: recovery_code.token, password: "q1234567", password_confirmation: "q1234567" }}}

    5.times{|i| let!("recovery_password#{i}") { create(:recovery_password, token: recovery_code.token) }}

    it 'should return status 404' do
      post '/api/recovery_password', headers: headers, params: params
      expect(json_response).to match(too_many_attemps_schema)
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  after { VCR.eject_cassette }
end
