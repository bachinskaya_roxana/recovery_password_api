# frozen_string_literal: true
require 'rails_helper'

describe 'Check Login Credentials', type: :request do
  let!(:user) { create(:user, email: "dev@mlsdev.com") }
  let(:params) {{ 'email' => "dev@mlsdev.com" }}
  let(:headers) do
    {
      'ACCEPT'            => 'application/json',
      'HTTP_CONTENT_TYPE' => 'application/json'
    }
  end

  let(:schema) do
    {
      id: be_kind_of(Integer),
      email: be_kind_of(String),
      phone: be_kind_of(String)
    }
  end

  let(:schema_not_found) do
    {
      errors: {"base"=>["Not found"]}
    }
  end

  let(:schema_too_many_attempts) do
    {
      errors: {"base"=>["too many attempts today"]}
    }
  end

  let(:invalid_email_schema) do
    {
      errors: {"email"=>["is invalid"]}
    }
  end

  describe 'check_login_credentials with email when user exist' do
    let!(:user) { create(:user, email: "dev@mlsdev.com", status: 'active') }

    it 'should return user' do
      post '/api/check_login_credentials', headers: headers, params: params

      expect(json_response).to match(schema)
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'check_login_credentials with email when user doesn\'t exist' do
    let(:params) {{ 'email' => "bachinskaya@mlsdev.com" }}

    it 'should return 404 and send code to email' do
      post '/api/check_login_credentials', headers: headers, params: params

      expect(json_response).to match(schema_not_found)
      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'check_login_credentials with phone when user doesn\'t exist' do
    let(:params) {{ 'phone' =>  "+130989997865" }}

    before { VCR.insert_cassette 'twilio_request' }

    it 'should return 404 and send code to phone' do
      post '/api/check_login_credentials', headers: headers, params: params

      expect(json_response).to match(schema_not_found)
      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'check_login_credentials when to many attempts' do
    let(:params) {{ 'email' =>  "johnny@gmail.com" }}
    let!(:user) { create :user }

    before { VCR.insert_cassette 'twilio_request' }

    5.times {|i| let!("recovery_code#{i}") { create :recovery_code, email: "johnny@gmail.com", kind: :email, user: user }}

    it 'should return 422 with error message' do
      post '/api/check_login_credentials', headers: headers, params: params

      expect(json_response).to match(schema_too_many_attempts)
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe 'check_login_credentials when incorrect email' do
    let(:params) {{ 'email' =>  "johnnygmail.com" }}

    before { VCR.insert_cassette 'twilio_request' }

    it 'should return 422 and send code to phone' do
      post '/api/check_login_credentials', headers: headers, params: params

      expect(json_response).to match(invalid_email_schema)
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  after { VCR.eject_cassette }
end
