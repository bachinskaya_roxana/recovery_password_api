# frozen_string_literal: true
require 'rails_helper'

describe 'Recovery Code Request', type: :request do
  let!(:user) { create(:user, email: "dev@mlsdev.com") }
  let(:params) {{ 'email' => "dev@mlsdev.com" }}
  let(:headers) do
    {
      'ACCEPT'            => 'application/json',
      'HTTP_CONTENT_TYPE' => 'application/json'
    }
  end

  describe 'recovery code with email' do
    it 'should return success status without body and send code to email' do
      post '/api/recovery_code', headers: headers, params: params

      expect(response).to have_http_status(:no_content)
    end
  end

  describe 'recovery code with email when user doesn\'t exist' do
    let(:params) {{ email: "wrong_email@mlsdev.com" }}

    it 'should return status 204 and not send code' do
      post '/api/recovery_code', headers: headers, params: params

      expect(response).to have_http_status(:no_content)
    end
  end

  describe 'recovery code with phone' do
    let!(:user) { create(:user, phone: "+130989997865") }
    let(:params) {{ 'phone' =>  "+130989997865" }}

    before { VCR.insert_cassette 'twilio_request' }

    it 'should return status 204 and send code to phone' do
      post '/api/recovery_code', headers: headers, params: params

      expect(response).to have_http_status(:no_content)
    end
  end

  after { VCR.eject_cassette }
end
