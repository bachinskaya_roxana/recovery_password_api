# frozen_string_literal: true
require 'rails_helper'

describe Api::CheckLoginCredentialsService, type: :service do
  describe '.save!' do
    let(:request) { ActionDispatch::Request.new(env = {}) }

    subject { Api::CheckLoginCredentialsService.new(request, phone: "+19876543212") }

    context 'success' do

      before { VCR.insert_cassette 'twilio_request' }

      it { expect { subject.save! }.to_not raise_error }

      after { VCR.eject_cassette }
    end

    context 'failure - incorrect email' do
      let(:request) { ActionDispatch::Request.new(env = {}) }

      subject { Api::CheckLoginCredentialsService.new(request, email: "johnnygmail.com") }

      before { VCR.insert_cassette 'twilio_request' }

      it { expect { subject.save! }.to raise_error ActiveRecord::RecordInvalid }

      after { VCR.eject_cassette }
    end
  end
end
