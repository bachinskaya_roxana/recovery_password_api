# frozen_string_literal: true
require 'rails_helper'

describe Api::RecoveryPasswordService, type: :service do
  describe '.save!' do
    let!(:user) { create(:user, phone: "+19876543212")}

    let(:recovery_code) { create(:recovery_code, token: "12345", phone: "+19876543212", kind: :phone, user: user)}

    let(:request) { ActionDispatch::Request.new(env = {}) }

    let(:params) { { token: recovery_code.token, password: "password", password_confirmation: "password" } }

    subject { Api::RecoveryPasswordService.new(request, params) }

    context 'success' do
      before { VCR.insert_cassette 'twilio_request' }

      it { expect { subject.save! }.to_not raise_error }

      after { VCR.eject_cassette }
    end

    context 'failure - invalid password confirmation' do
      let(:params) { { token: recovery_code.token, password: "password", password_confirmation: "pass" } }

      subject { Api::RecoveryPasswordService.new(request, params) }

      before { VCR.insert_cassette 'twilio_request' }

      it { expect { subject.save! }.to raise_error(ActiveModel::StrictValidationFailed) }

      after { VCR.eject_cassette }
    end

    context 'failure - invalid token' do
      let(:params) { { token: "124", password: "password", password_confirmation: "password" } }

      subject { Api::RecoveryPasswordService.new(request, params) }

      before { VCR.insert_cassette 'twilio_request' }

      it { expect { subject.save! }.to raise_error(ActiveModel::StrictValidationFailed) }

      after { VCR.eject_cassette }
    end
  end
end
