# frozen_string_literal: true
require 'rails_helper'

describe Api::RecoveryCodeValidationService, type: :service do
  describe '.save!' do
    let!(:user) { create(:user, phone: "+19876543212")}

    let(:recovery_code) { create(:recovery_code, token: "12345", phone: "+19876543212", kind: :phone, user: user)}

    let(:request) { ActionDispatch::Request.new(env = {}) }

    subject { Api::RecoveryCodeValidationService.new(request, token: recovery_code.token) }

    context 'success' do
      before { VCR.insert_cassette 'twilio_request' }

      it { expect { subject.save! }.to_not raise_error }

      after { VCR.eject_cassette }
    end
  end
end
