# frozen_string_literal: true
require 'rails_helper'

describe Api::RecoveryCodeService, type: :service do
  describe '.save!' do
    let!(:user) { create(:user, phone: "+19876543212")}

    let(:request) { ActionDispatch::Request.new(env = {}) }

    subject { Api::RecoveryCodeService.new(request, user, phone: "+19876543212") }

    context 'success' do
      before { VCR.insert_cassette 'twilio_request' }

      it { expect { subject.save! }.to_not raise_error }

      after { VCR.eject_cassette }
    end

    context 'failure - incorrect email' do
      let(:request) { ActionDispatch::Request.new(env = {}) }

      subject { Api::RecoveryCodeService.new(request, nil, email: "johnnygmail.com") }

      before { VCR.insert_cassette 'twilio_request' }

      it { expect { subject.save! }.to raise_error ActiveModel::StrictValidationFailed }

      after { VCR.eject_cassette }
    end
  end
end
