# frozen_string_literal: true

describe ApplicationJob, type: :job do
  it { should be_a ActiveJob::Base }
end
