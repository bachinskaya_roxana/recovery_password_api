# frozen_string_literal: true
require 'rails_helper'

describe RecoveryCodeDecorator, type: :decorator do
  describe '#as_json' do
    let!(:user) { create(:user, email: "dev@mlsdev.com") }
    let!(:recovery_code) { create(:recovery_code, kind: :email, user: user, email: user.email) }

    subject { recovery_code.decorate.as_json }

    its([:recovery_password_type]) { should eq(recovery_code.kind) }
  end
end
