# frozen_string_literal: true
require 'rails_helper'

describe UserDecorator, type: :decorator do
  describe '#as_json' do
    let!(:user) { create(:user, phone: "+180923456789") }

    subject { user.decorate.as_json }

    its([:id]) { should eq(user.id) }

    its([:phone]) { should eq(user.phone) }

    its([:email]) { should eq(user.email) }
  end
end
