# Recovery password API

Sign up flow with code and recovery password API

## Technologies

|Technology            |Version               |Notes                                                                         |
|----------------------|:--------------------:|------------------------------------------------------------------------------|
|Ruby                  |2.6.2                 |                                                                              |
|Ruby on Rails         |5.2.2.1                 |                                                                              |

### How to run app

* set user in `database.yml` or remain it default

* create `.env` file with Twilio configurations

* set smtp configuaration in `.env` file

* set the email default from in `mailer/application_mailer.rb`

* run `gem install bundler`

* run `bundle`

* run `rake db:create db:migrate`

* run `rspec` for running tests

* run `rails s`

## API Docs

* POST `/check_login_credentials` - first step of sign up with code

body: `{email: string}` or `{phone: string}`

example:

```
curl -X POST "http://localhost:3000/api/check_login_credentials" -H "accept: application/json" -H "Content-Type: application/json" -d "email=john.lenon%40gmail.com"
```

response: 200 - User already exist

response body:
```
{
  "id": 1,
  "email": "john.lenon%40gmail.com",
  "phone": "+390987865432"
}
```

response: 404 - User is new - sending code for continue registration on hpone or email

* POST `/recovery_code`

body: `{email: string}` or `{phone: string}`

example:
```
curl -X POST "http://localhost:3000/api/recovery_code" -H "Accept: application/json" -H "Content-Type: application/json" -d "email=john.lenon%40gmail.com"
```

response: 204 - sending code


* POST `/recovery_code/validation` - validate recovery/confirmation code

body: {token: string}

example:
```
curl -X POST "http://localhost:3000/api/recovery_code/validation" -H "accept: application/json" -H "Content-Type: application/json" -d "token=50053"
```

response: 200
response body:
{
  "recovery_password_method": "phone/email"
}

response: 404 - Code is invalid
response: 422 - Code is expired

* POST `/recovery_password` - set password for account if token is valid (password should consist 8 or more digits)

body:
```
{
  recovery_password: {
    token: string,
    password: string,
    password_confirmation
  }
}
```

example:
```
curl -X POST "http://localhost:3000/api/recovery_password" -H "accept: application/json" -H "Content-Type: application/json" -d "recovery_password%5Btoken%5D=121212&recovery_password%5Bpassword%5D=23232323&recovery_password%5Bpassword_confirmation%5D=23232323"
```

response: 204 - Password has been set successfully
response: 422 - errors

Possible errors:

```
  base: ['token is expired']
  base: ['invalid_token']
  base: ['too many attepts']
  password: ['is too short (minimum is 8 characters)']
  password_confirmation: ['doesn't match password']
```

Errors model example:
```
{
  "errors": {
    "base": [
      "Email and/or password is invalid",
      "Too many attempts"
    ],
    "email": [
      "Invalid email"
    ]
  }
}
```



