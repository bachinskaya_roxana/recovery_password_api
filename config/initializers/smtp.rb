Rails.application.configure do
  config.action_mailer.smtp_settings = {
    address:              'smtp.gmail.com',
    port:                 587,
    domain:               ENV["SMTP_DOMAIN"],
    authentication:       'plain',
    enable_starttls_auto: true,
    user_name:            ENV["SMTP_USERNAME"],
    password:             ENV["SMTP_PASSWORD"]
  }
end
