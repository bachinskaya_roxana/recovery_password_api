# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "kure-backend"
set :repo_url, "git@git.mlsdev.com:kure/kure-backend.git"
set :rails_env, :production
set :migration_role, :app

set :deploy_to, '/srv/kure/backend'

set :linked_files, ['.env']

folders = %w[log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system]
set :linked_dirs, fetch(:linked_dirs) { [] }.push(*folders)

namespace :deploy do
  after :finishing, 'systemd:restart'
  after :finishing, 'bundler:clean'
end
