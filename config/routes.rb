Rails.application.routes.draw do
  namespace :api do
    resources :docs, only: :index
    resource :check_login_credentials, only: :create
    resource :recovery_password,       only: [:show, :create]
    resource :recovery_code,           only: :create
    namespace :recovery_code do
      resource :validation, only: :create
    end
  end
end
