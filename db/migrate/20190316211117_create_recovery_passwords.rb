class CreateRecoveryPasswords < ActiveRecord::Migration[5.2]
  def change
    create_table :recovery_passwords do |t|
      t.string     :remote_ip
      t.string     :token
      t.datetime   :expire_at
      t.references :user, index: true, foreign_key: true
      t.integer    :status, default: 0
      t.integer    :reason

      t.timestamps
    end

    add_index :recovery_passwords, :expire_at
  end
end
