class CreateRecoveryCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :recovery_codes do |t|
      t.string     :remote_ip
      t.string     :token
      t.string     :email
      t.string     :phone
      t.integer    :kind
      t.integer    :status, default: 0
      t.integer    :validated_times, default: 0
      t.datetime   :validated_last_at
      t.datetime   :expire_at
      t.references :user, index: true, foreign_key: true

      t.timestamps
    end

    add_index :recovery_codes, :token, unique: true
    add_index :recovery_codes, :expire_at
    add_index :recovery_codes, :validated_times
    add_index :recovery_codes, :validated_last_at
    add_index :recovery_codes, :kind
    add_index :recovery_codes, :email
  end
end
