class CreateRecoveryCodeValidations < ActiveRecord::Migration[5.2]
  def change
    create_table :recovery_code_validations do |t|
      t.string :remote_ip
      t.string :token

      t.timestamps
    end

    add_index :recovery_code_validations, :remote_ip
  end
end
