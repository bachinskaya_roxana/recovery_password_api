class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :phone
      t.string :email
      t.string :password_digest
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
