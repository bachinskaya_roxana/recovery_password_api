# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_20_093013) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "recovery_code_validations", force: :cascade do |t|
    t.string "remote_ip"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["remote_ip"], name: "index_recovery_code_validations_on_remote_ip"
  end

  create_table "recovery_codes", force: :cascade do |t|
    t.string "remote_ip"
    t.string "token"
    t.string "email"
    t.string "phone"
    t.integer "kind"
    t.integer "status", default: 0
    t.integer "validated_times", default: 0
    t.datetime "validated_last_at"
    t.datetime "expire_at"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_recovery_codes_on_email"
    t.index ["expire_at"], name: "index_recovery_codes_on_expire_at"
    t.index ["kind"], name: "index_recovery_codes_on_kind"
    t.index ["token"], name: "index_recovery_codes_on_token", unique: true
    t.index ["user_id"], name: "index_recovery_codes_on_user_id"
    t.index ["validated_last_at"], name: "index_recovery_codes_on_validated_last_at"
    t.index ["validated_times"], name: "index_recovery_codes_on_validated_times"
  end

  create_table "recovery_passwords", force: :cascade do |t|
    t.string "remote_ip"
    t.string "token"
    t.datetime "expire_at"
    t.bigint "user_id"
    t.integer "status", default: 0
    t.integer "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["expire_at"], name: "index_recovery_passwords_on_expire_at"
    t.index ["user_id"], name: "index_recovery_passwords_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "phone"
    t.string "email"
    t.string "password_digest"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "recovery_codes", "users"
  add_foreign_key "recovery_passwords", "users"
end
