# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

gem 'pg', '>= 0.18', '< 2.0'
gem 'rails', '~> 5.2.2.1'
gem 'dotenv-rails'
gem 'puma', '~> 3.11'
gem 'bcrypt', '~> 3.1.7'
gem 'draper'
gem 'pg_search'
gem 'swagger-blocks'
gem 'rack-cors'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'email_validator'
gem 'twilio-ruby', '~> 5.21.2'

group :development, :test do
  gem 'awesome_print'
  gem 'bullet'
  gem 'factory_bot_rails'
  gem 'ffaker'
  gem 'pry-rails'
  gem 'rspec-rails', '~> 3.8'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'database_rewinder'
  gem 'shoulda-callback-matchers'
  gem 'rails-controller-testing'
  gem 'rspec-its'
  gem 'shoulda-matchers'
  gem 'simplecov', require: false
  gem 'vcr'
  gem 'webmock'
end
